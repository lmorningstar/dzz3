import express from 'express';
import mongoose from 'mongoose';
import session from 'express-session';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import bluebird from 'bluebird';
import fetch from 'isomorphic-fetch';

import config from './config';
import errorHandler from './middlewares/errorHandler';
import getData from './middlewares/getData';
import learnRoute from './routes/lernRoute';
import lodashRoute from './routes/lodashRoute';

import User from './models/user';
import Pet from './models/pets';

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

const app = express();

mongoose.Promise = bluebird;
mongoose.connect(config.database, (err) => {
  if (err) throw err;
  console.log(`Mongoose connect to database "${config.database}"`);
  fetch(pcUrl)
    .then(async(resourse) => {
      const pc = await resourse.json();
      await User.remove();
      await Pet.remove();
      pc['users'].forEach(async function(item) {
        item._id = item.id;
        item.pets = [];
        await User.create(item);
      });
      pc['pets'].forEach(async function(item) {
        item._id = item.id;
        item.user = item.userId;
        const pet = await Pet.create(item);
        await User.update({
          id: item['userId']
        }, {
          $push: {
            pets: pet
          }
        });
      });
      console.log('get remote data');
    })
    .catch((err) => {
      throw err;
    });
});

app.listen(config.port, (err) => {
  if (err) throw err;

  console.log(`API server runing on port ${config.port}`);
});

app.use(cors());
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.secret,
}));


app.use('/blackbox', (req, res) => {
  const { i } = req.query;

  switch (i) {
    case '0':
      return res.send('1');
    case '1':
      return res.send('18');
    case '2':
      return res.send('243');
    case '3':
      return res.send('3240');
    case '4':
      return res.send('43254');
    case '5':
      return res.send('577368');
    case '6':
      return res.send('7706988');
    case '7':
      return res.send('102876480');
    case '8':
      return res.send('1373243544');
    case '9':
      return res.send('18330699168');
    case '10':
      return res.send('244686773808');
    case '11':
      return res.send('3266193870720');
    case '12':
      return res.send('43598688377184');
    case '13':
      return res.send('581975750199168');
    case '14':
      return res.send('7768485393179328');
    case '15':
      return res.send('103697388221736960');
    case '16':
      return res.send('1384201395738071424');
    case '17':
      return res.send('18476969736848122368');
    case '18':
      return res.send('246639261965462754048');
    case '19':
      return res.send('243');
    default:
      return res.status(404).send('Not Found');
  }


});

app.get('/', async(req, res) => {
  const users = await User.find({}, {
    _id: 0,
    pets: 0
  }).sort('id');
  const pets = await Pet.find({}, {
    _id: 0,
    user: 0
  }).sort('id');
  return res.send({
    users,
    pets
  });
});

//app.use('/', lodashRoute);
//app.use('/', learnRoute);

// app.use('/api', pageRoute);
// app.use('/api', checkToken, getUser, userRoute);

app.use(errorHandler);
