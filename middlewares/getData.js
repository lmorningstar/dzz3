import fetch from 'isomorphic-fetch';
import User from '../models/user';
import Pet from '../models/pets';

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

export default async (req, res, next) => {
  let pc;
  fetch(pcUrl)
    .then(async (resourse) => {
      pc = await resourse.json();
      await User.remove({});
      await Pet.remove({});
      pc['users'].forEach(async function (item) {
        item._id = item.id;
        const user = await User.create(item);
      });
      pc['pets'].forEach(async function (item) {
        item._id = item.id;
        const pet = await Pet.create(item);
      });
      console.log('get remote data');
      return next();
    })
    .catch((err) => {
      return res.status(404).send('Not Found');
    });
};
