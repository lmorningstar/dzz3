import Page from '../models/page';

export const getUserPages = async (user) => {
  const { _id } = user;
  let pages = [];

  try {
    pages = await Page.find({ userId: _id });
  } catch (e) {
    throw e;
  }

  return pages;
};

export const getPageByURL = async (url) => {
  let pages;

  try {
    pages = await Page.find({ url }).populate('userId', 'login');
  } catch (e) {
    throw e;
  }

  return pages;
};

export const getPageByAuthor = async (author) => {
  let pages = [];

  try {
    pages = await Page.find({ userId: author._id }).populate('userId', 'login');
  } catch (e) {
    throw e;
  }

  return pages;
};

export const getAllPages = async () => {
  let pages = [];

  try {
    pages = await Page.find({}).populate('userId', 'login');
  } catch (e) {
    throw e;
  }

  return pages;
};

export const editPage = async (data) => {
  const { _id, credentials } = data;

  const page = await Page.update({ _id }, credentials);

  return page;
};
