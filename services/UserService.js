import User from '../models/user';

export const getUserByToken = async (token) => {
  const { _id } = token;
  let user;

  try {
    user = await User.findOne({ _id }, { password: 0 });
  } catch (e) {
    throw e;
  }

  return user;
};

export const getAllUsers = async () => {
  let users = [];

  try {
    users = await User.find({}, { password: 0 });
  } catch (e) {
    throw e;
  }

  return users;
};

export const editUser = async (data) => {
  const { _id, credentials } = data;

  const user = await User.update({ _id }, credentials);

  return user;
};
