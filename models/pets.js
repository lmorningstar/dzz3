import mongoose, { Schema } from 'mongoose';

const states = ['dog', 'cat', 'rat'];

const PetsSchema = new Schema({
  _id: {
    type: Number,
    required: true
  },
  id: Number,
  userId: {
    type: Number,
    ref: 'User',
  },
  type: {
    type: String,
    enum: states,
    index: true,
  },
  color: String,
  age: Number,
  user: {
    type: Number,
    ref: 'User',
  },
}, {
  versionKey: false,
});

export default mongoose.model('Pets', PetsSchema);
