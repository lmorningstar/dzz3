import mongoose, { Schema } from 'mongoose';

const UserSchema = new Schema({
  _id: {
    type: Number,
    required: true
  },
  id: {
    type: Number,
    index: true,
  },
  username: {
    type: String,
    index: true,
  },
  fullname: String,
  password: String,
  values: {
    money: String,
    origin: String,
  },
  pets: [{ type: Number, ref: 'Pets' }],
}, {
  autoIndex: false,
  versionKey: false,
});

export default mongoose.model('User', UserSchema);
