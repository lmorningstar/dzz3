import express from 'express';

import User from '../models/user';
import Pet from '../models/pets';

const router = express.Router();

router.get('/users', async (req, res) => {
  const { havePet } = req.query;
  if (!havePet) {
    const users = await User.find({}, { _id: 0, pets: 0 }).sort('id');
    return res.json(users);
  } else {
    let users = [];
    const pets = await Pet
      .find({ type: havePet }, { _id: 0 })
      .populate('userId', { _id: 0, pets: 0 });
    pets.forEach((item) => {
      let good = true;
      users.forEach((itemU) => {
        if (itemU['id'] === item['userId']['id']) {
          good = false;
        }
      });
      if (good) {
        users.push(item['userId']);
      }
    });
    users.sort((a, b) => {
      return a.id - b.id;
    });
    return res.send(users);
  }
});

router.get('/users/:id', async (req, res) => {
  const id = req.params.id;
  let user;

  if (+id) {
    user = await User.findOne({ id: id }, { _id: 0, pets: 0 });
  } else {
    user = await User.findOne({ username: id }, { _id: 0, pets: 0 });
  }

  if (!user) {
    return res.status(404).send('Not Found');
  }

  return res.send(user);
});

router.get('/pets', async (req, res) => {
  let { type, age_gt, age_lt } = req.query;
  if (!age_gt) age_gt = 0;
  if (!age_lt) age_lt = 99999;

  let answer;
  if (type) {
    answer = await Pet.find({
      type: type,
      age: { $gt: age_gt, $lt: age_lt },
    }, { _id: 0, user: 0 }).sort('id');
  } else {
    answer = await Pet.find({
      age: { $gt: age_gt, $lt: age_lt },
    }, { _id: 0, user: 0 }).sort('id');
  }

  return res.json(answer);
});

router.get('/pets/populate', async (req, res) => {
  let { type, age_gt, age_lt } = req.query;
  if (!age_gt) age_gt = 0;
  if (!age_lt) age_lt = 99999;

  let answer;
  if (type) {
    answer = await Pet.find({
      type: type,
      age: { $gt: age_gt, $lt: age_lt },
    }, { _id: 0 }).sort('id').populate('user', { _id: 0, pets: 0 });
  } else {
    answer = await Pet.find({
      age: { $gt: age_gt, $lt: age_lt },
    }, { _id: 0 }).sort('id').populate('user', { _id: 0, pets: 0 });
  }

  return res.send(answer);
});

router.get('/pets/:id/populate', async (req, res) => {
  const id = req.params.id;

  const pets = await Pet.findOne({ id }, { _id: 0 }).populate('user', { _id: 0, pets: 0 });
  if (!pets) {
    return res.status(404).send('Not Found');
  }
  return res.send(pets);
});

router.get('/pets/:id', async (req, res) => {
  const id = req.params.id;

  const pets = await Pet.findOne({ id }, { _id: 0, user: 0 });
  if (!pets) {
    return res.status(404).send('Not Found');
  }
  return res.send(pets);
});

export default router;
