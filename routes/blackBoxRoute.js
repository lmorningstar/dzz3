import express from 'express';

const router = express.Router();

router.get('/blackbox', (req, res) => {
  const { i } = req.query;
  let j = +i;

  j += 1;

  return res.send(j);
});

export default router;
