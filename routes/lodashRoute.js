import express from 'express';
import _ from 'lodash';

const router = express.Router();

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';
let pets = {};
fetch(pcUrl)
  .then(async(res) => {
    pets = await res.json()
  })
  .catch(err => {
    console.log('Что-то пошло не так:', err);
  });

router.get('/users/:id/populate', async (req, res) => {
  let result;
  const arrUsers = _.cloneDeep(pets.users);
  _.forEach(arrUsers, (valueUsers, keyPets) => {
    if (_.filter(pets.pets, ['userId', valueUsers.id])) {
      valueUsers.pets = _.filter(pets.pets, ['userId', valueUsers.id]);
    }
  });
  result = _.find(arrUsers, ['id', parseInt(req.params.id)]);
  if (result === 0 || !result) {
    result = _.find(arrUsers, ['username', req.params.id]);
  }
  if (result) {
    return res.json(result);
  }
  return res.status(404).send('Not Found');
});

router.get('/users/populate', async (req, res) => {
  let result;
  let arrUsers = _.cloneDeep(pets.users);
  let arrSearch = [];
  _.forEach(arrUsers, (valueUsers, keyPets) => {
    if (_.filter(pets.pets, ['userId', valueUsers.id])) {
      valueUsers.pets = _.filter(pets.pets, ['userId', valueUsers.id]);
    }
  });
  result = arrUsers;
  if (req.query.havePet) {
    _.forEach(arrUsers, (valueUsers, keyPets) => {
      _.forEach(valueUsers.pets, (valuePets, keyUsers) => {
        if (valuePets.type === req.query.havePet && !_.find(arrSearch, ['id', valueUsers.id])) {
          arrSearch.push(valueUsers);
        }
      });
    });
    result = arrSearch;
  }
  if (result) {
    result = _.sortBy(result, ['id']);
    return res.json(result);
  } else {
    return res.status(404).send('Not Found');
  }
});

module.exports = router;
